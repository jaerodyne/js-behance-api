## Installation
    git clone https://jaerodyne@bitbucket.org/jaerodyne/js-behance-api.git
    yarn install
    yarn start

Then navigate to website:``http:localhost:3000/``.

## Troubleshooting
The Behance API isn't configured for CORS, so a proxy has to be used to make requests to the endpoints. A public proxy is available at <https://cors-anywhere.herokuapp.com/>. 

However, there is a limit on requests that can be made to that url, so I've provided an alternate url where I uploaded my own proxy at <https://desolate-reaches-28479.herokuapp.com/>. It's commented out and is located in this repo at src/actions/index.js on line 3. Try this url in case the website reaches the request limit and stops returning the proper JSON data.

---

# Dealer Inspire Front End Code Challenge

Hey - welcome to the Dealer Inspire Javascript Code challenge.  We're glad you're here.

## Getting Started

First of all, you'll need to fork this repository to somewhere else.  Make sure your code is publicly available in a git repository when you're done.   (Like Bitbucket or GitHub. If you're super-nerdy and host your own public repo, just make sure the server is up 24/7 - our code-review monkeys like to work in the middle of the night.)

You don't have to host a working copy of the code, we'll be checking it out locally to review it. That's a good reminder - we'll need you to update this document with instructions on how to run your project locally (whether it's a yarn, npm or ng style command, we're fine - just tell us with one!)

## The Challenge

Behance is one of the most popular websites for front-end and art portfolios.  For this code challenge, you'll be using the Behance JSON API to build out a quick front-end for a search users and profile display website.  To get started, visit here: [Behance Developers](https://www.behance.net/dev).

- The first view of the app should have a search box where searches for users are executed
- When a user is chosen/found, the next view is an individual profile page for the user. The profile page should include:

	- basic information about the user (including stats)
	- a list of their projects with links to them (links should be exterior to your app and link directly to Behance)
	- a list of their work experience,
	- a preview of their followers and following lists.

## Expectations

You provide and code an original, usable, and contemporary design.

You use your favorite front-end javascript framework to create the REST API requests, but do **not** use the Behance JS library itself. (Normally we don't like to re-invent the wheel, but in this case, we want to see how you wrangle the JSON directly.)

You provide unit and/or end-to-end tests for your code.

import React, { Component } from 'react';
import Home from './Home'

class App extends Component {
  render() {
    return (
      <div>
        <header>
          <h1>Find the artist.</h1>
        </header>
        <Home />
      </div>
    );
  }
}

export default App;

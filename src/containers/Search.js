import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsers } from '../actions';

import SearchBar from 'material-ui-search-bar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class Search extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	searchInput: ''
	  };

	  this.onChange = this.onChange.bind(this)
	  this.onRequestSearch = this.onRequestSearch.bind(this)
	}

	onChange(input) {
		this.setState({searchInput: input})
	}

	onRequestSearch() {
		this.props.getUsers(this.state.searchInput)
	}

	render() {
		return (
			 <MuiThemeProvider>
        <SearchBar
          onChange={(input) => this.onChange(input)}
          onRequestSearch={() => this.onRequestSearch()}
	          style={{
	            margin: '0 auto',
	            maxWidth: 800,
	            marginBottom: 10
	          }}
        />
        </MuiThemeProvider>
		);
	}
}

Search.propTypes = {
	searchInput: PropTypes.string
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getUsers }, dispatch)
}

export default connect(null, mapDispatchToProps)(Search);
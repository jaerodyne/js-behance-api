import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserData, getProjects, getWorkExperience, getFollowers, getFollowing } from '../actions';
import CountUp from 'react-countup';

import UserProjects from '../components/UserProjects';
import UserWorkExperience from '../components/UserWorkExperience';
import UserFollowersList from '../components/UserFollowersList';
import UserFollowingList from '../components/UserFollowingList'; 

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
	avatar: {
		margin: 5
	},
	root: {
	  display: 'flex',
	  flexWrap: 'wrap',
	  justifyContent: 'space-around',
	  textAlign: 'center'
	}
};

class User extends Component {

	constructor(props) {
		super(props);
	
		this.state = {
			dataLoaded: false,
			userId: null
		};
	}

	componentDidMount(){
		const userId = this.props.match.params.id
		this.props.getUserData(userId)
		this.props.getProjects(userId)
		this.props.getWorkExperience(userId)
		this.props.getFollowers(userId)
		this.props.getFollowing(userId)
	}

	componentWillUnmount(){
		// this.props.clearData();
	}

	renderData = ({data}) => {
		if(data) {
			const user = data.user

			const basicInfo = (
				
				<List>
					<ListItem
						disabled={true}
					>
						<Avatar 
							src={user.images["276"]}
							alt={'No img found :('}
		          size={200}
		          style={styles.avatar}
						/>
						<h2>{user.display_name}</h2>
						<h4>{user.username}</h4>
						<ul className="profile-info-list">
							{user.location ? <li>{user.location}</li> : null}
							{user.company ? <li>{user.company}</li> : null }	
							{user.occupation ? <li>{user.occupation}</li> : null}	
							<li><a href={`${user.url}`} className="profile-link">{user.url}</a></li>	
						</ul>
					</ListItem>
				</List>
			)

			const stats = (
				<ul className="stats">
					<li>
						<CountUp start={0} end={`${user.stats.followers}`} />
						<h5>FOLLOWERS</h5>
					</li>
					<li>
						<CountUp start={0} end={`${user.stats.following}`} />
						<h5>FOLLOWING</h5>
					</li>
					<li>
						<CountUp start={0} end={`${user.stats.appreciations}`} />
						<h5>APPRECIATIONS</h5>
					</li>
					<li>
						<CountUp start={0} end={`${user.stats.views}`} />
						<h5>VIEWS</h5>
					</li>
					<li>
						<CountUp start={0} end={`${user.stats.comments}`} />
						<h5>COMMENTS</h5>
					</li>
				</ul>
			)

			const teamMembers = (
				<div className="stats-team-members">
					<h5>TEAM MEMBERS</h5>
					<h5>{user.stats.team_members ? user.stats.team_members : ' None found'}</h5>
				</div>
			)

			return (
					<MuiThemeProvider>
						<div>
								<div style={styles.root}>
									{basicInfo}
								</div>
								<div style={styles.root}>
									{stats}
								</div>
								<div style={styles.root}>
									{teamMembers}
								</div>
						</div>
					</MuiThemeProvider>
			)
		} else {
			return (
				<MuiThemeProvider>
					<div style={styles.root}>
						<CircularProgress size={60} thickness={5} />
					</div>
				</MuiThemeProvider>
			)
		}
	}
	
	render() {  
		return (
			<div>
				{this.renderData(this.props.userData)}
				<UserWorkExperience work={this.props.work} />
				<UserProjects projects={this.props.projects}/>
				<UserFollowersList followers={this.props.followers} />
				<UserFollowingList following={this.props.following} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		userData: state.users,
		projects: state.users.projects,
		work: state.users.work,
		followers: state.users.followers,
		following: state.users.following
	}
}


function mapDispatchToProps(dispatch){
	return bindActionCreators({ getUserData, getProjects, getWorkExperience, getFollowers, getFollowing }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(User);
import React, { Component } from 'react';

import Search from './Search';
import UsersList from '../components/UsersList';

class Home extends Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
      loading: false
    };

    this.setLoading = this.setLoading.bind(this)
  }

  setLoading = () => {
    this.setState({
      loading: true
    })
  }

  render() {
    return (
      <div>
        <Search />
        <UsersList />
      </div>
    );
  }
}

export default Home;

import { GET_USERS, GET_USER_DATA, GET_PROJECTS, GET_WORK_EXPERIENCE, GET_FOLLOWERS, GET_FOLLOWING, CLEAR_DETAIL } from '../actions';

export default function(state={},action) {
	switch(action.type){
		case GET_USERS:
			return {...state, list:action.payload}
		case GET_USER_DATA:
			return {...state, data:action.payload}
		case GET_PROJECTS:
			return {...state, projects:action.payload}
		case GET_WORK_EXPERIENCE:
			return {...state, work:action.payload}
		case GET_FOLLOWERS:
			return {...state, followers:action.payload}	
		case GET_FOLLOWING:
			return {...state, following:action.payload}
		case CLEAR_DETAIL:
		 return {...state, detail:action.payload}
		default:
			return state
	}
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { CircularProgress, GridList, GridTile } from 'material-ui/GridList';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 800,
    height: 500,
    overflowY: 'auto',
  },
};

class UsersList extends Component {
	
	usersList = ({users}) => {
		if(users && users.length > 0){
			return(
				<GridList
					cols={3}
					cellHeight={280}
					style={styles.gridList}
				>
					{users.map((user)=>{
						return(
							<Link key={user.id} to={`/user/${user.id}`}>
								<GridTile
									title={user.display_name}
									subtitle={<span>{user.username}</span>}
								>
									<img src={user.images["276"]} alt="NO IMG FOUND :("/>
								</GridTile>
							</Link>
						)
					})}
				</GridList>
			)
		} else if (users && users.length === 0) {
			return <h2 className="no-match">No match found.</h2>
		} else {
			return (
				<div>
					<h3>Loading users</h3>
				 	<CircularProgress size={60} thickness={7} />
				</div>
			)
		}
	}

	render() {
		return (
			<div style={styles.root}>
			{this.props.usersList ?
				<MuiThemeProvider>
					{this.usersList(this.props.usersList)}
				</MuiThemeProvider> 
				: null}
			</div>
		);
	}
}

function mapStateToProps(state) {
	console.log(state.users.list)
	return {
		usersList: state.users.list
	}
}

export default connect(mapStateToProps, null)(UsersList);
import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {GridList, GridTile} from 'material-ui/GridList';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 1000,
    height: 450,
    overflowY: 'auto',
  },
};

const renderProjects = ({projects}) => {
	if(projects) {
		return (
		projects.projects.map((project) => {
			return (
				<GridTile
					key={project.id}
					title={project.name}
					subtitle={<a href={`${project.url}`}>{project.url}</a>}
				>
					<img src={project.covers.original} alt="No Img Found :("/>
				</GridTile>
			)
		}))
	} else {
		return (
			<MuiThemeProvider>
				<CircularProgress size={60} thickness={5} />
			</MuiThemeProvider>
		)
	}
}

const UserProjects = (props) => {
	return (
		<div>
			<h1>PROJECTS</h1>
			<div style={styles.root}>
				<MuiThemeProvider>
					<GridList
						cellHeight={200}
						cols={3}
						style={styles.gridList}
					>
						{renderProjects(props)}
					</GridList>
				</MuiThemeProvider>
			</div>
		</div>
	);
}

export default UserProjects;
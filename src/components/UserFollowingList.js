import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {GridList, GridTile} from 'material-ui/GridList';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
  }
};

const renderFollowing = ({following}) => {
	if(following) {
		return (
			<GridList style={styles.gridList} cols={1}>
			{following.following.map((follow) => {
				return (
					<GridTile 
						key={follow.id}
						title={
							<a href={`${follow.url}`}>
								{follow.display_name}
							</a>
						}
						subtitle={
							<p>
								{follow.username} <br/> 
								{follow.location}
							</p>}
						>
						<img src={follow.images["276"]} alt="NO IMG FOUND :("/>
					</GridTile>
				)
			})}
			</GridList>
		)
	} else {
		return (
			<MuiThemeProvider>
				<CircularProgress size={60} thickness={5} />
			</MuiThemeProvider>
		)
	}
}

const UserFollowingList = (props) => {
	return (
		<div>
			<h1>FOLLOWING</h1>
			<MuiThemeProvider>
				{renderFollowing(props)}
			</MuiThemeProvider>
		</div>
	);
}

export default UserFollowingList;
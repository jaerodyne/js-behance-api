import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {GridList, GridTile} from 'material-ui/GridList';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
  }
};

const renderFollowers = ({followers}) => {
	if(followers) {
		return (
			<GridList style={styles.gridList} cols={1}>
				{followers.followers.map((follower) => {
					return (
						<GridTile 
							key={follower.id}
							title={
								<a href={`${follower.url}`}>
									{follower.display_name}
								</a>
							}
							subtitle={
								<p>
									{follower.username} <br/> 
									{follower.location}
								</p>}
						>
							<img src={follower.images["276"]} alt="NO IMG FOUND :("/>
						</GridTile>
					)
				})}
			</GridList>
		)
	} else {
		return (
			<MuiThemeProvider>
				<CircularProgress size={60} thickness={5} />
			</MuiThemeProvider>
		)
	}
}

const UserFollowersList = (props) => {
	return (
		<div>
			<h1>FOLLOWERS</h1>
			<div style={styles.root}>
				<MuiThemeProvider>
					{renderFollowers(props)}
				</MuiThemeProvider>
			</div>
		</div>
	);
}

export default UserFollowersList;
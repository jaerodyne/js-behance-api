import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
	root: {
	  display: 'flex',
	  flexWrap: 'wrap',
	  justifyContent: 'space-around',
	  textAlign: 'center'
	}
};

const renderWork = ({work}) => {
	if(work && work.length > 0) {
		return (
			<MuiThemeProvider>
				{work.work_experience.map((experience, index) => {
					return (
						<div key={index}>
							<h3>{experience.organization}</h3>
							<p>{experience.position}</p>
							<p>{experience.location}</p>
						</div>
					)
				})}
			</MuiThemeProvider>
		)
	} else if (work && work.valid === 0) {
		return (
			<div style={styles.root}>
				<h2 className="no-match">No work experience added.</h2> 
			</div>
		)
	} else {
		return (
			<div style={styles.root}>
				<MuiThemeProvider>
				 	<CircularProgress size={60} thickness={7} />
			 	</MuiThemeProvider>
			</div>
		)
	}
}

const UserWorkExperience = (props) => {
	return (
		<div>
			
				<h1>EXPERIENCE</h1>

				{renderWork(props)}
		</div>
	);
}

export default UserWorkExperience;
const URL = `https://www.behance.net/v2`
const proxyurl = 'https://cors-anywhere.herokuapp.com/';
// const proxyurl = 'https://desolate-reaches-28479.herokuapp.com/'

export const GET_USERS = 'GET_USERS'
export const GET_USER_DATA = 'GET_USER_DATA'
export const GET_PROJECTS = 'GET_PROJECTS'
export const GET_WORK_EXPERIENCE = 'GET_WORK_EXPERIENCE'
export const GET_FOLLOWERS = 'GET_FOLLOWERS'
export const GET_FOLLOWING = 'GET_FOLLOWING'
export const CLEAR_DETAIL = 'CLEAR_DETAIL'
		
export function getUsers(searchInput) {

	const request = fetch(`${proxyurl}${URL}/users?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB&q=${searchInput}`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_USERS,
		payload: request
	}
}

export function getUserData(id) {
	
	const request = fetch(`${proxyurl}${URL}/users/${id}?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_USER_DATA,
		payload: request
	}
}

export function getProjects(id) {

	const request = fetch(`${proxyurl}${URL}/users/${id}/projects?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_PROJECTS,
		payload: request
	}
}

export function getWorkExperience(id) {

	const request = fetch(`${proxyurl}${URL}/users/${id}/work_experience?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_WORK_EXPERIENCE,
		payload: request
	}
}


export function getFollowers(id) {

	const request = fetch(`${proxyurl}${URL}/users/${id}/followers?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_FOLLOWERS,
		payload: request
	}
}


export function getFollowing(id) {

	const request = fetch(`${proxyurl}${URL}/users/${id}/following?client_id=L37Gl0TqyhtWMya3uDqzn57n4v4m1RXB`, {
		method: 'GET'
	})
	.then(response => response.json())
	.catch((error) => {
   	return error
  })

	return {
		type: GET_FOLLOWING,
		payload: request
	}
}

export function clearDetail(){
    return{
      type: CLEAR_DETAIL,
      payload:[]
    }
}
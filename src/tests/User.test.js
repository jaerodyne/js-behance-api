import React from 'react';
import { shallow } from 'enzyme';

import { User } from '../containers/User';

describe('User', () => {
	const user = shallow(<User />);

	it('renders User correctly', () => {
		expect(user).toMatchSnapshot();
	});

	it('contains a UserProjects component', () => {
		expect(app.find('UserProjects').exists()).toBe(true);
	});

	it('contains a UserWorkExperience component', () => {
		expect(app.find('UserProjects').exists()).toBe(true);
	});

	it('contains a UserFollowersList component', () => {
		expect(app.find('UserFollowersList').exists()).toBe(true);
	});

	it('contains a UserFollowingList component', () => {
		expect(app.find('UserFollowingList').exists()).toBe(true);
	});

})
import reducer from '../reducers/users_reducer';
import * as constants from '../actions';

describe('users_reducer', () => {
	it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({})
  });
})
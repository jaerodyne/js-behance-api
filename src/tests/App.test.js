import React from 'react';
import { shallow } from 'enzyme';

import App from '../containers/App';

describe('App', () => {
	const app = shallow(<App />);

	it('renders App correctly', () => {
		expect(app).toMatchSnapshot();
	});

	it('contains a Home component', () => {
		expect(app.find('Home').exists()).toBe(true);
	});
});
import React from 'react';
import { shallow } from 'enzyme';

import { Search } from '../containers/Search';

describe('Search', () => {
	const search = shallow(<Search />);

	it('renders Search correctly', () => {
		expect(search).toMatchSnapshot();
	});
});
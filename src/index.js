import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route } from 'react-router-dom';
import promiseMiddleware from 'redux-promise';
import './style/index.css';

import App from './containers/App';
import User from './containers/User';
import reducers from './reducers';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';

const iconStyles = {
  margin: 14,
  fontSize: '36px',
  color: 'white'
};

const createStoreWithMiddleware = applyMiddleware(promiseMiddleware)(createStore);

ReactDOM.render(
	<Provider store={createStoreWithMiddleware(reducers)}>
		<BrowserRouter>
    	<div>
	    	<MuiThemeProvider>
	    	  <nav className="nav">
	    	    <FontIcon className="material-icons" style={iconStyles}>search</FontIcon>
	    	    <h3><a href="/">BEHANCE SEARCH</a></h3>
	    	  </nav>
	    	</MuiThemeProvider>
        <Route exact path="/" component={App}></Route>
        <Route exact path="/user/:id" component={User}></Route>
     	</div>
   </BrowserRouter>
	</Provider>
	, document.getElementById('root'));